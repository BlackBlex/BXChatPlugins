/**
 * FontChooser
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package fontChooser
 *
 * ==============Information==============
 *      Filename: FontChooser.java
 * ---------------------------------------
*/

package fontChooser;

import java.awt.Font;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.swing.DefaultListModel;

public class FontChooser extends javax.swing.JDialog
{

	private static final long serialVersionUID = -5712780485146706008L;
	private int fontStyle = Font.PLAIN, fontSize = 12;
    private final Map<String, Integer> fontStyleList = new LinkedHashMap<>();
    private DefaultListModel<String> jListStyleModel = new DefaultListModel<>();

    public FontChooser()
    {
        initComponents();

        this.fontStyleList.put("Normal", Font.PLAIN);
        this.fontStyleList.put("Negrita", Font.BOLD);
        this.fontStyleList.put("Cursiva", Font.ITALIC);
        this.fontStyleList.put("Negrita Cursiva", Font.BOLD | Font.ITALIC);
        this.fontStyleList.put("Subrayado", 4);

        this.getListFontStyle();

        this.jListStyle.setModel(this.jListStyleModel);
        this.jListSize.setSelectedIndex(0);
        this.jListStyle.setSelectedIndex(0);

        this.setModal(true);

        this.setLocationRelativeTo(null);

    }

    private void getListFontStyle()
    {
        this.jListStyleModel = new DefaultListModel<>();

        for ( String style : this.fontStyleList.keySet())
            this.jListStyleModel.addElement(style);
    }

    private int getSelectedFontStyle()
    {
        if ( this.jListStyle.getSelectedValue() != null)
            return this.fontStyleList.get(this.jListStyle.getSelectedValue());
        return Font.PLAIN;
    }

    private int getSelectedFontSize()
    {
        return Integer.parseInt(this.jListSize.getSelectedValue());
    }

    private void changeFontPre()
    {
        fontStyle = getSelectedFontStyle();
        fontSize = getSelectedFontSize();
        Font font;
        if ( fontStyle != 4)
        {
            font = new Font("Arial", fontStyle, fontSize);
            jLabelPreview.setFont(font);
            jLabelPreview.setText("AaBbYyZz123#$%&");
        }
        else if ( fontStyle == 4)
        {
            font = new Font("Arial", 0, fontSize);
            jLabelPreview.setFont(font);
            jLabelPreview.setText("<html><u>AaBbYyZz123#$%&</u></html>");
        }
    }

    public String showDialog()
    {
        this.setVisible(true);
        return fontStyle + "-" + fontSize;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jPanel1 = new javax.swing.JPanel();
        jLabelPreview = new javax.swing.JLabel();
        jButtonAccept = new javax.swing.JButton();
        jButtonCancel = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListStyle = new javax.swing.JList<>();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jListSize = new javax.swing.JList<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Seleccione el estilo");
        setResizable(false);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Previsualizaci�n"));

        jLabelPreview.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabelPreview.setText("AaBbYyZz123#$%&");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelPreview, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabelPreview, javax.swing.GroupLayout.DEFAULT_SIZE, 91, Short.MAX_VALUE)
        );

        jButtonAccept.setText("Aceptar");
        jButtonAccept.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonAcceptActionPerformed(evt);
            }
        });

        jButtonCancel.setText("Cancelar");
        jButtonCancel.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCancelActionPerformed(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Estilo"));

        jListStyle.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jListStyle.addListSelectionListener(new javax.swing.event.ListSelectionListener()
        {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt)
            {
                jListStyleValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jListStyle);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 92, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Tama�o"));

        jListSize.setModel(new javax.swing.AbstractListModel<String>()
        {
            String[] strings = { "8", "9", "10", "11", "12", "14", "16", "18", "20", "22", "24", "26", "28", "36", "48", "72" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jListSize.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jListSize.addListSelectionListener(new javax.swing.event.ListSelectionListener()
        {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt)
            {
                jListSizeValueChanged(evt);
            }
        });
        jScrollPane3.setViewportView(jListSize);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 97, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButtonAccept, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonCancel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButtonAccept, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 19, Short.MAX_VALUE)
                        .addComponent(jButtonCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jListStyleValueChanged(javax.swing.event.ListSelectionEvent evt)
    {
        changeFontPre();
    }

    private void jListSizeValueChanged(javax.swing.event.ListSelectionEvent evt)
    {
        changeFontPre();
    }

    private void jButtonCancelActionPerformed(java.awt.event.ActionEvent evt)
    {
        fontSize = 999;
        fontStyle = 999;
        this.dispose();
    }

    private void jButtonAcceptActionPerformed(java.awt.event.ActionEvent evt)
    {
        fontStyle = getSelectedFontStyle();
        fontSize = getSelectedFontSize();
        Font font;
        if ( fontStyle != 4)
        {
            font = new Font("Arial", fontStyle, fontSize);
            jLabelPreview.setFont(font);
            jLabelPreview.setText("AaBbYyZz123#$%&");
        }
        else if ( fontStyle == 4)
        {
            font = new Font("Arial", 0, fontSize);
            jLabelPreview.setFont(font);
            jLabelPreview.setText("<html><u>AaBbYyZz123#$%&</u></html>");
        }
        this.dispose();
    }

    private javax.swing.JButton jButtonAccept;
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JLabel jLabelPreview;
    private javax.swing.JList<String> jListSize;
    private javax.swing.JList<String> jListStyle;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
}

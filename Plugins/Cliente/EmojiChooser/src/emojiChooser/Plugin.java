/**
 * EmojiChooser
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package emojiChooser
 *
 * ==============Information==============
 *      Filename: Plugin.java
 * ---------------------------------------
*/

package emojiChooser;

import com.blackblex.libs.application.components.borders.RoundedSidesBorder;
import com.blackblex.libs.application.components.customs.jLabelImage;
import com.blackblex.libs.system.utils.Images;
import com.blackblex.libs.system.utils.Observable;
import com.blackblex.libs.system.utils.Version;
import com.blackblex.libs.system.utils.Version.Status;
import com.blackblex.plugins.core.Core;
import com.blackblex.plugins.core.PluginCore;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JLabel;
import javax.swing.JTextPane;

public class Plugin extends PluginCore {

	private String name = "EmojiChooser",
			author = "BlackBlex",
			description = "Agrega un selector de emojis";

	private final Version version = new Version(0, 0, 1, 0, Status.ALPHA);

	private Core.TYPE type = Core.TYPE.COMPLEMENT;
	public JTextPane jMessage;

	@Override
	public boolean load() {
		Core.Message.printlnStatus(getName().toUpperCase(), getDescription());

		jMessage = (JTextPane) Core.globalService.getObject("JMESSAGE");

		JLabel emojiButton = new JLabel();
		emojiButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		emojiButton.setIcon(Images.getImagen("/resources/images/emoji.png", emojiButton, new Dimension(45, 45)));
		emojiButton.setBorder(new RoundedSidesBorder(Color.darkGray, 1, 10, 10));
		emojiButton.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Emojis emoji = new Emojis();
				jLabelImage emojiSelect = emoji.showDialog();

				if (emojiSelect != null) {
					jMessage.setCaretPosition(jMessage.getStyledDocument().getLength());
					jMessage.insertComponent(new JLabel(" "));
					jMessage.insertComponent(emojiSelect);
					jMessage.insertComponent(new JLabel(" "));
					jMessage.setAlignmentY(1.0f);
				}
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseReleased(MouseEvent e) {
			}

		});

		Core.globalService.addToContainer("jPanelPluginContainer1", emojiButton);

		return true;
	}

	@Override
	public boolean start() {
		return true;
	}

	@Override
	public boolean end() {
		return true;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public String getAuthor() {
		return this.author;
	}

	@Override
	public String getDescription() {
		return this.description;
	}

	@Override
	public Version getVersion() {
		return this.version;
	}

	@Override
	public Core.TYPE getType() {
		return this.type;
	}

	@Override
	public void update(Observable obj) {

	}
}

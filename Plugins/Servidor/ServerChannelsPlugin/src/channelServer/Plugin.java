package channelServer;

import com.blackblex.libs.system.utils.Observable;
import com.blackblex.libs.system.utils.Version;
import com.blackblex.libs.system.utils.Version.Status;
import com.blackblex.plugins.core.Core;
import com.blackblex.plugins.core.PluginCore;

public class Plugin extends PluginCore {

	private String name = "ChannelsPlugin",
			author = "BlackBlex",
			description = "Da la opcion de tener canales en el chat";

	private final Version version = new Version(0, 0, 1, 0, Status.ALPHA);

	private Core.TYPE type = Core.TYPE.COMPLEMENT;

	@Override
	public boolean load() {

		return true;
	}

	@Override
	public boolean start() {
		return true;
	}

	@Override
	public boolean end() {
		return true;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public String getAuthor() {
		return this.author;
	}

	@Override
	public String getDescription() {
		return this.description;
	}

	@Override
	public Version getVersion() {
		return this.version;
	}

	@Override
	public Core.TYPE getType() {
		return this.type;
	}

	@Override
	public void update(Observable obj) {

	}

}

**BXChat Plugins**
===================


Aquí se alojaran todos los plugins disponibles para [BXChat Client](https://gitlab.com/BlackBlex/bxchatclient) y [BXChat Server](https://gitlab.com/BlackBlex/bxchatserver)

*Importante:* La rama (main) contiene codigo solo para ejecutar en Java 9 en adelante, si quiere usar Java 8, use la rama Java 8

--------
**Plugins disponibles:**

 - **SendPMMessage** ~ Implementa un chat privado. [Client/Server]
 - **SendFile** ~ Agrega la posibilidad de mandar archivos a un usuario en especifico. [Client/Server]
 - **EmojiChooser** ~ Agrega un selector de emojis. [Client]
 - **FontChooser** ~ Agrega la posibilidad de cambiar el tamaño, el estilo de la fuente. [Client]

--------

**Nota:** *Se irán añadiendo más plugins conforme se vayan desarrollando*



--------
 BXChat | Chat básico con sistema de plugins

 Plugins escritos en java

 Author: @BlackBlex (BlackBlex)

 License: General Public License (GPLv3) | http://www.gnu.org/licenses/

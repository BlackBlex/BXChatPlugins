/**
 * ServerSendFile
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package plugin
 *
 * ==============Information==============
 *      Filename: Plugin.java
 * ---------------------------------------
*/

package sendFile;

import com.blackblex.libs.core.interfaces.custom.CommandInterface;
import com.blackblex.libs.system.utils.Observable;
import com.blackblex.libs.system.utils.Version;
import com.blackblex.libs.system.utils.Version.Status;
import com.blackblex.plugins.core.Core;
import com.blackblex.plugins.core.PluginCore;
import java.util.Map;

public class Plugin extends PluginCore {

	private String name = "SendFile",
			author = "BlackBlex",
			description = "Manda un archivo a un usuario";

	private final Version version = new Version(0, 0, 1, 0, Status.ALPHA);

	private Core.TYPE type = Core.TYPE.COMPLEMENT;

	@Override
	public boolean load() {
		Core.Message.printlnStatus("COMMAND", "New command");
		Core.Message.printlnStatus(getName().toUpperCase(), getDescription());
		Core.Message.printlnStatus("USAGE", "[sendfile] [file]");

		Map<String, CommandInterface> commands = (Map<String, CommandInterface>) Core.globalService
				.getObject("COMMANDLIST");
		commands.put("sendFile", new SendFile());

		return true;
	}

	@Override
	public boolean start() {
		return true;
	}

	@Override
	public boolean end() {
		return true;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public String getAuthor() {
		return this.author;
	}

	@Override
	public String getDescription() {
		return this.description;
	}

	@Override
	public Version getVersion() {
		return this.version;
	}

	@Override
	public Core.TYPE getType() {
		return this.type;
	}

	@Override
	public void update(Observable obj) {

	}
}

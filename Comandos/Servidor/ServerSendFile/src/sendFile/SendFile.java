/**
 * ServerSendFile
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package plugin
 *
 * ==============Information==============
 *      Filename: SendFile.java
 * ---------------------------------------
*/

package sendFile;

import com.blackblex.libs.core.interfaces.custom.CommandInterface;
import com.blackblex.libs.net.objects.SocketMessage;
import com.blackblex.libs.net.objects.SocketUsername;
import com.blackblex.plugins.core.Core;
import java.util.Map;

public class SendFile implements CommandInterface {

	@Override
	public String getDescription() {
		return "";
	}

	@Override
	public boolean canExecute(SocketUsername socketUsername) {
		return true;
	}

	@Override
	public void execute(SocketUsername socketUsername, SocketMessage dataInput) {
		SocketUsername user = ((Map<Integer, SocketUsername>) Core.globalService.getObject("CLIENTSONLINE"))
				.get(dataInput.getTo());
		user.sendClient(dataInput);
	}

}

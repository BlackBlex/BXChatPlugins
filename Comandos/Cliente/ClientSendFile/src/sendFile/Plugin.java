/**
 * ClientSendFile
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package sendFile
 *
 * ==============Information==============
 *      Filename: Plugin.java
 * ---------------------------------------
*/

package sendFile;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import com.blackblex.libs.core.interfaces.custom.CommandInterface;
import com.blackblex.libs.net.objects.SocketMessage;
import com.blackblex.libs.net.objects.SocketUsername;
import com.blackblex.libs.net.objects.jMessage;
import com.blackblex.libs.system.utils.Observable;
import com.blackblex.libs.system.utils.Version;
import com.blackblex.libs.system.utils.Version.Status;
import com.blackblex.plugins.core.Core;
import com.blackblex.plugins.core.JPanelPluginContainer;
import com.blackblex.plugins.core.PluginCore;

import sendPMMessage.ChatFramePM;

public class Plugin extends PluginCore {

	private String name = "SendFile",
			author = "BlackBlex",
			description = "Manda un archivo a un usuario";

	private final Version version = new Version(0, 0, 1, 0, Status.ALPHA);

	private Core.TYPE type = Core.TYPE.COMPLEMENT;

	private SocketUsername socketServer;

	private boolean jButtonFile = false;
	public static DefaultListModel<SocketUsername> jListUserModel;
	private SocketUsername user = null;

	@Override
	public boolean load() {
		SocketMessage dataInput = (SocketMessage) Core.globalService.getObject("DATAINPUT");
		this.socketServer = (SocketUsername) Core.globalService.getObject("SOCKETSERVER");
		jListUserModel = (DefaultListModel<SocketUsername>) Core.globalService.getObject("JLISTUSERSMODEL");

		dataInput.add(this);

		Core.Message.printlnStatus("COMMAND", "New command");
		Core.Message.printlnStatus(getName().toUpperCase(), getDescription());
		Core.Message.printlnStatus("USAGE", "[sendfile] [file]");

		Map<String, CommandInterface> commands = (Map<String, CommandInterface>) Core.globalService
				.getObject("COMMANDLIST");
		commands.put("sendFile", new SendFile());

		return true;
	}

	@Override
	public boolean start() {
		addRequiered("SendPMMessage");
		return true;
	}

	@Override
	public boolean end() {
		return true;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public String getAuthor() {
		return this.author;
	}

	@Override
	public String getDescription() {
		return this.description;
	}

	@Override
	public Version getVersion() {
		return this.version;
	}

	@Override
	public Core.TYPE getType() {
		return this.type;
	}

	@Override
	public void update(Observable obj) {
		if (!jButtonFile) {
			String pars[] = obj.actionString().split("-/-");

			String action = pars[0];
			int to = Integer.parseInt(pars[1]);

			for (int i = 0; i < Plugin.jListUserModel.getSize(); i++) {
				user = (SocketUsername) Plugin.jListUserModel.getElementAt(i);
				if (user.getIdSession() == to) {
					break;
				}
			}

			ArrayList<sendPMMessage.ChatFramePM> chatMPS = (ArrayList<ChatFramePM>) Core.globalService
					.getObject("CHATSMP");
			List<jMessage> jMessagesChat = (List<jMessage>) Core.globalService.getObject("JMESSAGESCHAT");

			List<Component> componentsFrame;

			JButton sendFileButton = new JButton("Enviar archivo");

			sendFileButton.setName("jButtonSendFile");
			sendFileButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					String fileEncode = "";
					byte[] fileRead;
					try {
						JFileChooser jFileChooser1 = new JFileChooser();
						jFileChooser1.showOpenDialog(null);

						File abre = jFileChooser1.getSelectedFile();

						if (abre != null) {
							fileRead = Files.readAllBytes(Paths.get(abre.getAbsolutePath()));
							fileEncode = Base64.getEncoder().encodeToString(fileRead);

							jMessage jmes = new jMessage(2);
							jmes.setMessage("Enviaste el siguiente archivo \"" + abre.getName() + "\" al usuario: "
									+ user.getUsername());
							jMessagesChat.add(jmes);
							SocketMessage dataOutput = new SocketMessage();
							dataOutput.setAction("sendFile");
							dataOutput.setFrom(socketServer.getIdSession());
							dataOutput.setTo(to);
							dataOutput.setMessage(abre.getName());
							dataOutput.setFileName(abre.getName());
							dataOutput.setFileContent(fileEncode);
							dataOutput.setFilePath(abre.getPath());
							dataOutput.setFileSize(abre.length());
							socketServer.sendServer(dataOutput);

						}
					} catch (IOException ex) {
						JOptionPane.showMessageDialog(null, ex + ""
								+ "\nNo se ha encontrado el archivo",
								"ADVERTENCIA!!!", JOptionPane.WARNING_MESSAGE);
					}
				}

			});

			if (action.contains("sendpm")) {
				for (sendPMMessage.ChatFramePM chat : chatMPS) {
					componentsFrame = getAllComponents(chat.getContentPane());

					for (Component c : componentsFrame) {
						try {
							if (c.getName().contains("jPanelPluginContainer1")
									|| c.getClass().getCanonicalName().contains("jPanelPluginContainer1")) {
								boolean foundJButton = false;
								boolean userCorrect = false;
								JPanelPluginContainer jpcont = (JPanelPluginContainer) c;
								List<Component> componentPlugin = getAllComponents(jpcont);
								for (Component cp : componentPlugin) {
									try {
										if (cp.getName().contains("jButtonSendFile")
												|| cp.getClass().getCanonicalName().contains("jButtonSendFile")) {
											foundJButton = true;
											break;
										}
										if (chat.getUserMP().getIdSession() == to) {
											userCorrect = true;
											break;
										}
									} catch (NullPointerException ex) {
									}
								}

								if (!foundJButton && userCorrect) {
									GridBagConstraints gbc = new GridBagConstraints();
									gbc.fill = GridBagConstraints.BOTH;
									jpcont.add(sendFileButton, gbc);
									jpcont.revalidate();
									jpcont.updateUI();
									jButtonFile = true;
									break;
								}
							}
						} catch (NullPointerException ex) {
						}

					}

				}
			}
		}

	}
}

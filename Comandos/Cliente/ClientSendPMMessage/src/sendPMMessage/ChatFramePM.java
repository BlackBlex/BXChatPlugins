/**
 * ClientSendPMMessage
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package sendPMMessage
 *
 * ==============Information==============
 *      Filename: ChatFramePM.java
 * ---------------------------------------
*/

package sendPMMessage;

import com.blackblex.libs.net.objects.SocketMessage;
import com.blackblex.libs.net.objects.SocketUsername;
import com.blackblex.libs.net.objects.jMessage;
import com.blackblex.libs.system.utils.table.jMessageCell;
import com.blackblex.libs.system.utils.table.jMessageModel;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;

public class ChatFramePM extends javax.swing.JFrame
{
	private static final long serialVersionUID = -5016086908262557045L;
	public List <jMessage> jMessagesChat;
	public jMessageModel jMessageModel;

	private JLabel juserID = new JLabel();

	private SocketUsername userMP = null;

	public ChatFramePM()
	{
		initComponents();

		jTableMessages.setDefaultRenderer(jMessage.class, new jMessageCell());
		jTableMessages.setDefaultEditor(jMessage.class, new jMessageCell());

		this.juserID.setName("jUSERID");
		this.juserID.setVisible(false);

		jPanelPluginContainer1.add(this.juserID);
		jPanelPluginContainer1.revalidate();
		jPanelPluginContainer1.updateUI();

		jTableMessages.setRowHeight(100);
		jTableMessages.setShowGrid(false);
		jTableMessages.setRowMargin(4);

		jMessagesChat = new ArrayList <jMessage>();
		this.setTitle("Chat de " + Plugin.socketServer.getUsername());
		this.setLocationRelativeTo(null);
	}

	@SuppressWarnings ("unchecked")
	// <editor-fold defaultstate="collapsed" desc="Generated
	// Code">//GEN-BEGIN:initComponents
	private void initComponents()
	{
		java.awt.GridBagConstraints gridBagConstraints;

		jPanel1 = new javax.swing.JPanel();
		jScrollPane1 = new javax.swing.JScrollPane();
		jTableMessages = new javax.swing.JTable();
		jPanelPluginContainer1 = new com.blackblex.plugins.core.JPanelPluginContainer();
		jLabel1 = new javax.swing.JLabel();
		jScrollPane3 = new javax.swing.JScrollPane();
		jMessage = new javax.swing.JTextPane();
		jButton1 = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
		setTitle("Chat");
		setResizable(false);
		addWindowListener(new java.awt.event.WindowAdapter()
		{
			public void windowClosing(java.awt.event.WindowEvent evt)
			{
				formWindowClosing(evt);
			}
		});
		getContentPane().setLayout(new javax.swing.OverlayLayout(getContentPane()));

		jPanel1.setBackground(new java.awt.Color(255, 255, 255));

		jTableMessages.setBackground(new java.awt.Color(204, 204, 204));
		jTableMessages.setModel(new javax.swing.table.DefaultTableModel(new Object [] [] {

		}, new String [] {

		}));
		jTableMessages.setName("jTableMessages"); // NOI18N
		jScrollPane1.setViewportView(jTableMessages);

		jPanelPluginContainer1.setBackground(new java.awt.Color(255, 255, 255));
		jPanelPluginContainer1.setName("jPanelPluginContainer1"); // NOI18N

		jLabel1.setText("Mensaje:");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		jPanelPluginContainer1.add(jLabel1, gridBagConstraints);

		jScrollPane3.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		jScrollPane3.setViewportView(jMessage);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridwidth = 2;
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.weighty = 1.0;
		gridBagConstraints.insets = new java.awt.Insets(0, 4, 0, 4);
		jPanelPluginContainer1.add(jScrollPane3, gridBagConstraints);

		jButton1.setText("Enviar");
		jButton1.addActionListener(new java.awt.event.ActionListener()
		{
			public void actionPerformed(java.awt.event.ActionEvent evt)
			{
				jButton1ActionPerformed(evt);
			}
		});
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 3;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		jPanelPluginContainer1.add(jButton1, gridBagConstraints);

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout
				.setHorizontalGroup(
						jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(jPanel1Layout.createSequentialGroup().addContainerGap()
										.addGroup(jPanel1Layout
												.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
												.addComponent(jPanelPluginContainer1,
														javax.swing.GroupLayout.DEFAULT_SIZE, 557, Short.MAX_VALUE)
												.addComponent(jScrollPane1))
										.addContainerGap()));
		jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup().addContainerGap()
						.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 357,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(jPanelPluginContainer1, javax.swing.GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE)
						.addContainerGap()));

		getContentPane().add(jPanel1);

		pack();
	}// </editor-fold>//GEN-END:initComponents

	private void formWindowClosing(java.awt.event.WindowEvent evt)
	{

		for ( ChatFramePM chat : Plugin.chatMPS )
		{
			if ( chat.juserID.getText().startsWith(this.juserID.getText()) )
			{
				Plugin.chatMPS.remove(chat);
				break;
			}
		}

		this.dispose();
	}

	private void jButton1ActionPerformed(java.awt.event.ActionEvent evt)
	{
		putMessage();
	}

	public SocketUsername getUserMP()
	{
		return userMP;
	}

	public void setUserMP(SocketUsername userMP)
	{
		this.userMP = userMP;
		this.juserID.setText(userMP.getIdSession() + "");
	}

	public void putMessage()
	{
		SocketMessage dataOutput = new SocketMessage();
		dataOutput.setFrom(Plugin.socketServer.getIdSession());
		dataOutput.setjMessage(jMessage.getDocument());
		dataOutput.setAction("sendpm");
		dataOutput.setTo(getUserMP().getIdSession());
		Plugin.socketServer.sendServer(dataOutput);

		jMessage jmes = new jMessage(0);
		jmes.setJMessage(dataOutput.getjMessage());
		jMessagesChat.add(jmes);
		setModel();

		jMessage.setText("");
	}

	public void setModel()
	{
		List <Object> items = new ArrayList <Object>();

		for ( Object i : jMessagesChat )
		{
			items.add(i);
		}

		jMessageModel = new jMessageModel(items);
		jTableMessages.setModel(jMessageModel);

		jTableMessages.changeSelection(items.size() - 1, 0, true, true);
	}

	private javax.swing.JButton jButton1;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JTextPane jMessage;
	private javax.swing.JPanel jPanel1;
	private com.blackblex.plugins.core.JPanelPluginContainer jPanelPluginContainer1;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JScrollPane jScrollPane3;
	private javax.swing.JTable jTableMessages;
}
